const Footer = () => {
  return (
    <footer className={`footer-container`}>
      <div className="footer-link">
        <a href="https://t.me/arknights_music_pro">Telegram</a>
      </div>
    </footer>
  );
};
export default Footer;
