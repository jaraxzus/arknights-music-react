import { useState } from "react";
import { toast } from "react-toastify";
import { useApp } from "../../contexts/AppContext";
import AlbumPriview from "./AlbumPreview";
import st from "./AlbumPreviewList.module.css";
import { useSearchParams } from "react-router-dom";

const AlbumsPreviewList = () => {
  const { albums, setAlbums } = useApp();
  const [currentPage, setCurrentPage] = useState(0);
  const albumsPerPage = 14;

  const albumUpdateCallBack = async (album) => {
    try {
      const updatedAlbum = albums.map((obj) =>
        obj.cid === album.cid ? { ...obj, ...album } : obj,
      );
      setAlbums(updatedAlbum);
    } catch (error) {
      toast.error("Error updating album");
      console.error("Error updating album", error);
    }
  };
  const handlePageClick = ({ selected }) => {
    setCurrentPage(selected);
  };

  if (!albums) {
    return <p>Loading...</p>;
  }

  const offset = currentPage * albumsPerPage;
  const currentAlbums = albums.slice(offset, offset + albumsPerPage);

  return (
    <div className={st.homePage}>
      <div className={st.albumList}>
        {currentAlbums.map((album) => (
          <AlbumPriview
            key={album.cid}
            album={album}
            albumUpdateCallBack={albumUpdateCallBack}
          />
        ))}
      </div>
      {albums.length > albumsPerPage && (
        <div className={st.paginationContainer}></div>
      )}
    </div>
  );
};
