FROM node:14 as build

WORKDIR /app

# Скопируйте зависимости и файлы приложения
COPY package*.json ./
RUN npm install
COPY . .

# Соберите приложение
RUN npm run build

# Используйте легкий образ с Nginx для создания конечного образа
FROM nginx:alpine

# Копируйте собранное приложение из предыдущего образа
COPY --from=build /app/build /usr/share/nginx/html

# Expose порт Nginx (по умолчанию 80)
EXPOSE 80

# Команда запуска Nginx
CMD ["nginx", "-g", "daemon off;"]
